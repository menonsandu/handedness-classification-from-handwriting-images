# README #

### What is this repository for? ###

* Given an image of handwriting, it predicts if its a right handed or left handed handwriting
### How do I get set up? ###

* Clone the repository
* Install openCV, scikit, xgboost and keras
* Detection.py and classifier.py has all the training and prediction functions
* tangent.py and curvature.py extracts tangent and curvature features respectively

### Contribution guidelines ###

* Ongoing computer graphics project

### Who do I talk to? ###

* Sandeep N Menon	:	menonsandu@gmail.com
