
# coding: utf-8

# In[13]:

from tangent import getTangentFeatures
from curvature import getCurvature
import os
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier


# In[ ]:




# In[ ]:





# In[ ]:




# In[14]:

def extract_features(Dir):
    cnt=0
    index={}
    labels = []
    data=pd.read_csv('data.csv')
    for label in data:
        labels.append(label)
    
    for name in data['Image']:
        index[name]=1

    for image_file in os.listdir(Dir):

        if len(image_file.split('.'))==1:
            continue
        img_path=Dir+image_file
        if index.has_key(img_path):
            continue


        print image_file,
        tangent_features=getTangentFeatures(img_path)
        curvature_feature=getCurvature(img_path,200)
        newRow=[(img_path,tangent_features[0],tangent_features[1],tangent_features[2],curvature_feature[0],-1)]

        df = pd.DataFrame.from_records(newRow, columns=labels)
        
        df.to_csv('data.csv', mode='a',header=False)

        index[image_file]=1

        cnt+=1
        return cnt




def getAllFeatures(img_path,thres):
    tangent_features=getTangentFeatures(img_path)
    curvature_feature=getCurvature(img_path,200)
    newRow=[tangent_features[0],tangent_features[1],tangent_features[2],curvature_feature[0]]
    return newRow

    


# In[31]:

def train_and_test():
    labels = []
    data=pd.read_csv('data.csv')
    for label in data:
        labels.append(label)
    train_data,test_data=train_test_split(data,test_size=0.3,stratify=data['Label'])
    Re
    X=train_data[labels[1:5]].values
    Y=train_data['Label'].values
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(10, 4), random_state=1)
    clf.fit(X, Y)
    
    result=[]
    test_data_Y=test_data['Label'].values
    for row in test_data.values:
        result.append(clf.predict([row[1:5]])[0])
    
    return clf,100.0*sum(result==test_data_Y)/len(test_data)


# In[32]:

def predict_result(img_dir,clf):
    newRow=getAllFeatures(img_dir,200)
    
    print newRow
    result=clf.predict([newRow])[0]
    print clf.predict_proba([newRow])
    return result




