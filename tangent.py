
# coding: utf-8

# In[2]:

import cv2
import skimage
from cv2 import imread
import matplotlib.pyplot as plt
from skimage.morphology import skeletonize 
import numpy as np
from scipy.misc import imsave


# In[ ]:




# In[20]:

import cv2
import skimage
from cv2 import imread
import matplotlib.pyplot as plt
from skimage.morphology import skeletonize 
import numpy as np
from scipy.misc import imsave
from scipy import stats

dim_X=300
dim_Y=300

# In[39]:

def makeSkeleton(img):
    for i in range(300):
        for j in range(300):
            if img[i][j]>=190:
                img[i][j]=0
            else:
                img[i][j]=1
                
    skeleton = skeletonize(img)
    imsave('skeleton.png',skeleton)
    
def make_black_white(img,flag):
    if flag:
        for i in range(300):
            for j in range(300):
                if img[i][j]>=90:
                    img[i][j]=255
                else:
                    img[i][j]=0
    else:
        for i in range(300):
            for j in range(300):
                if img[i][j]>=90:
                    img[i][j]=0
                else:
                    img[i][j]=255
    
    return img

def getCoord(img):
    cord=[]
    cnt=0
    for i in range(300):
        for j in range(300):
            if img[i][j]==0:
                cnt+=1
                if cnt%1==0:
                    cord.append([i,j])
    cord=sorted(cord)
    points=np.array(cord)
    return points

def getPointsInWindow(img,center,size):
    xc,yc=center
    tlx,tly=xc-size,yc-size
    brx,bry=xc+size,yc+size
    
    windowx=[]
    windowy=[]
    for x in range(tlx,brx+1):
        for y in range(tly,bry+1):
            if tlx<0 or tly<0 or brx>=dim_X or bry>=dim_Y:
                continue
                
            if img[x][y]==0:
                windowx.append(x)
                windowy.append(y)
                
            
            
    return windowx,windowy
    
    


# In[42]:




# In[48]:

def calcTangent(img):
    points=getCoord(img)

    N=5
    window_size=2*N+1
    neg_num=0
    pos_num=0
    neg_sum=0
    pos_sum=0
    for point in points:
        x,y=getPointsInWindow(img,point,window_size)
        if len(x)!=0 and len(y)!=0:
            slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)

            if slope>=0:
                pos_num+=1
                pos_sum+=slope
            elif slope<0:
                neg_num+=1
                neg_sum+=slope
    try:
        neg_avg=0
        pos_avg=0
        neg_fract=0
        pos_fract=0
        tot_num=neg_num+pos_num
        if neg_num==0:
            neg_avg=0
        else:
            neg_avg=neg_sum/neg_num

        if pos_num==0:
            pos_avg=0
        else:
            pos_avg=pos_sum/pos_num

        neg_fract=1.0*neg_num/(tot_num)
        pos_fract=1.0*pos_num/(tot_num)
        
        return pos_avg,neg_avg,pos_fract,neg_fract

    except:
        print "Div by zero"
        
    
    


# In[49]:

def getTangentFeatures(file_path):
    img=imread(file_path,0)
    img=cv2.resize(img,(dim_X,dim_Y))
    makeSkeleton(img)
    img=imread('skeleton.png',0)
    img=cv2.resize(img,(dim_X,dim_Y))
    img=make_black_white(img,False)
    cv2.imwrite('pic.png',img)
    
    return calcTangent(img)




# In[ ]:



