
# coding: utf-8

# In[5]:

import cv2
import skimage
from cv2 import imread
import matplotlib.pyplot as plt
from skimage.morphology import skeletonize 
import numpy as np
from scipy.misc import imsave
from scipy.interpolate import UnivariateSpline


# In[6]:

def makeSkeleton(img):
    for i in range(300):
        for j in range(300):
            if img[i][j]>=90:
                img[i][j]=0
            else:
                img[i][j]=1
                
    skeleton = skeletonize(img)
    imsave('skeleton.png',skeleton)


# In[7]:

def make_black_white(img,thresh):
    for i in range(300):
        for j in range(300):
            if img[i][j]>=thresh:
                img[i][j]=255
            else:
                img[i][j]=0
    return img


# In[8]:

def makeCanny(img):
    img=make_black_white(img,90)
    edge=cv2.Canny(img,100,200)
    
    for i in range(300):
        for j in range(300):
            if edge[i][j]>=90:
                edge[i][j]=0
            else:
                edge[i][j]=255
            
    return edge


# In[9]:

def getCoord(img):
    cord=[]
    cnt=0
    for i in range(300):
        for j in range(300):
            if img[i][j]==0:
                cnt+=1
                if cnt%1==0:
                    cord.append([i,j])
    cord=sorted(cord)
    points=np.array(cord)
    return points


# In[10]:

def getCurvature(img_path,thresh):
    
    img=imread(img_path,0)
    img=cv2.resize(img,(300,300))
    cv2.imwrite('temp.png',img)
    img=make_black_white(img,thresh)
    cv2.imwrite('pic.png',img)
    edge=makeCanny(img)
    cv2.imwrite('canny.png',edge)
    
    edge_points=getCoord(edge)
    window=5
    curvature_set=[]
    for point in edge_points:
        x,y=point
        background=0
        foreground=0
        
        if x>window-1 and x<300-window-1 and y>window-1 and y<300-window-1:
            for i in range(x-window,x+window):
                for j in range(y-window,y+window):
                    if img[i][j]==0:
                        foreground+=1
                    else:
                        background+=1
            #print x,y,1.0*(background-foreground)/(background+foreground)
            curvature_set.append(1.0*(background-foreground)/(background+foreground))
            
    curvature=np.array(curvature_set)
    return 1.0*sum(curvature>0)/len(curvature),1.0*sum(curvature<0)/len(curvature)




# In[ ]:



